from django.shortcuts import render, redirect
from accounts.models import UserLoginForm, UserSignupForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User
from django import forms


def user_login(request):
    if request.method == "POST":
        form = UserLoginForm(request.POST)
        if form.is_valid():

            user = authenticate(
                request,
                username=form.cleaned_data["username"],
                password=form.cleaned_data["password"],
            )
            if user is not None:
                login(request, user)
                return redirect("home")

    else:
        form = UserLoginForm()
    context = {"form": form}
    return render(request, "accounts/login.html", context)


def user_logout(request):
    logout(request)
    return redirect("login")


def user_signup(request):
    if request.method == "POST":
        form = UserSignupForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                user = User.objects.create_user(
                    username=username,
                    password=password,
                )
                login(request, user)
                return redirect("list_projects")
            else:
                raise forms.ValidationError("The passwords do not match.")

    else:
        form = UserSignupForm()

    context = {
        "form": form,
    }

    return render(request, "accounts/signup.html", context)
