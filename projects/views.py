from django.shortcuts import render, redirect
from projects.models import Project, ProjectForm
from django.contrib.auth.decorators import login_required


@login_required(login_url="login")
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {"projects": projects}
    return render(request, "projects/list.html", context)


@login_required(login_url="login")
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {"project": project}
    return render(request, "projects/show.html", context)


@login_required(login_url="login")
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
