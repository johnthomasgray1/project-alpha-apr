from django.db import models
from projects.models import Project
from django.conf import settings
from django import forms


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(blank=False, null=False)
    due_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        exclude = ("is_completed",)
